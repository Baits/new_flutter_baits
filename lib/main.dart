import 'package:flutter/material.dart';
import 'package:new_flutter_baits/coba/card.dart';
//import 'package:new_flutter_baits/coba/text.dart';
//import 'package:new_flutter_baits/main2.dart';
//import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Easy List'),
        ),
        body: Container(
          child: ListView(
            padding:
                const EdgeInsets.only(bottom: 8, left: 8, right: 8, top: 8),
            children: <Widget>[
              card(),
              card(),
              card(),
            ],
          ),
        ),
      ),
    );
  }
}
